import _ from 'lodash';

const settings = {
    appService: {
        baseUrl: {
            dev: 'https://whitelabel-dev.scout.com:3000',
            qa: 'https://whitelabel-qa.scout.com:3000',
            prod: 'https://whitelabel.scout.com:3000'
        }
    }
};

export default {
    forEnv(env) {
        return {
            get(id) {
                const result = _.get(settings, `${id}.${env}`);
                if (!result) {
                    throw new Error(`Missing setting id='${id}', env='${env}'`);
                }
                return result;
            }
        };
    }
};
