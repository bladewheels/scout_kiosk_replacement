import React from 'react';
import {Modal, Button, OverlayMixin} from 'react-bootstrap';
import VouchersMenu from './vouchersMenu.jsx';

const MainMenu = React.createClass({

    mixins: [OverlayMixin],

    getInitialState() {
        return {menuItemSelected: false, isModalOpen: false};
    },

    handleMenuClick(item) {
        console.log('Main menu.handleMenuClick('+item+') called, setting state...');
        this.setState({menuItemSelected: true, menuItem: item, isModalOpen: true});
    },

    render() {

        const styles = {
            menu: {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: '80vw',
                marginLeft: '10vw'
            },
            menuButton: {
                marginBottom: 10
            }
        };

        const mainMenu = <div className="btn-group-vertical" role="group" style={styles.menu} >
                            <h3>Your Points: 1675</h3>
                            <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'Points')} >Points</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'FoodAndVouchers')} >Food &amp; Vouchers</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton}>Pure Rewards Playback</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton}>Special Offers &amp; Events</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton}>Draws</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton}>Merchandise</button>
                        </div>;

        if (this.state.menuItemSelected) {
            console.log('Main menu.render('+this.state.menuItem+') called...');
            switch (this.state.menuItem) {
                case 'FoodAndVouchers':
                    return <VouchersMenu/>;
                case 'Points' :
                default:
                    return mainMenu;
            }
        } else {
            return mainMenu;
        }
    },

    // This is called by the `OverlayMixin` when this component
    // is mounted or updated and the return value is appended to the body.
    renderOverlay() {
        if (!this.state.isModalOpen) {
            return <span/>;
        }
        return this.getMenuItemModalContent(this.state.menuItem);
    },

    handleModalClose() {
        this.setState({menuItemSelected: false, isModalOpen: false});
    },

    getMenuItemModalContent(item) {

        const style = {
            content: {
                border: '1px solid silver',
                marginTop: 10,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'lightsteelblue'
            }
        };
        let content;

        switch (item) {
            case 'Points' :
                content = <div style={style.content} >
                                <p>Thank you, $[name], for using your PURE Rewards points card!</p>
                                <p>Total visits here at $[casinoName]: $[locationVisits]</p>
                                <p>PURE Rewards points available: $[points]</p>
                                <p>Attention PURE Rewards Members. Members points have been updated to account for unaccounted redemptions. If you have a concern about your point totals please ask to see a history of your visits and redemptions.</p>
                          </div>;
                break;
            default:
                content = <div>default</div>;
        }

        return (<div className='static-modal'>
            <Modal title={item}
                   enforceFocus={false}
                   backdrop={false}
                   animation={false}
                //container={mountNode}
                   onRequestHide={() => {}}>
                <div className='modal-body'>
                    {content}
                </div>
                <div className='modal-footer'>
                    <Button onClick={this.handleModalClose}>Close</Button>
                </div>
            </Modal>
        </div>);
    }
});

export default MainMenu;
