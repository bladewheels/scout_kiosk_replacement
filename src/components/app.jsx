import 'babel/polyfill';
import React from 'react';
import {IntlMixin} from '../lib/ReactIntlWrapper.js';
import Welcome from './welcome.jsx';

const App = React.createClass({

    propTypes: {
        config: React.PropTypes.shape({
            get: React.PropTypes.func.isRequired
        }).isRequired
    },

    mixins: [IntlMixin],

    render() {
        return (<Welcome location={'Edmonton'}/>);
    }
});

export default App;
