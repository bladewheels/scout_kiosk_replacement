import React from 'react';
import {IntlMixin, FormattedMessage} from 'react-intl';

const FormattedMessageWrapper = React.createClass({

    propTypes: {
        message: React.PropTypes.string
    },

    mixins: [IntlMixin],

    render() {
        return <FormattedMessage {...this.props} message={this.getIntlMessage(this.props.message)}/>;
    }
});

export default FormattedMessageWrapper;
