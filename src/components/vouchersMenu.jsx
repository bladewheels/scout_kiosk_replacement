import React from 'react';
import {Modal, Button, OverlayMixin} from 'react-bootstrap';

const VouchersMenu = React.createClass({

    mixins: [OverlayMixin],

    getInitialState() {
        return {menuItemSelected: false, isModalOpen: false};
    },

    handleMenuClick(item) {
        this.setState({menuItemSelected: true, menuItem: item});
    },

    render() {

        const styles = {
            menu: {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: '80vw',
                marginLeft: '10vw'
            },
            menuButton: {
                marginBottom: 10
            }
        };

        return (
            <div className="btn-group-vertical" role="group" style={styles.menu} >
                <h3>Your Points: 1675</h3>
                <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'tenPercentOff')} >10% off PURE Rewards voucher</button>
                <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'fiveDollars')} >$5 Voucher, for 1,000 PURE Rewards points</button>
                <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'tenDollars')} >$10 Voucher, for 2,000 PURE Rewards points</button>
            </div>
        );
    },


    // This is called by the `OverlayMixin` when this component
    // is mounted or updated and the return value is appended to the body.
    renderOverlay() {
        if (!this.state.isModalOpen) {
            return <span/>;
        } else {
            return this.getMenuItemModalContent(this.state.menuItem);
        }
    },

    handleModalClose() {
        this.setState({menuItemSelected: false, isModalOpen: false});
    },

    getMenuItemModalContent: (item) => {

        const style = {
            container: {
                border: '1px solid silver',
                marginTop: 10,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'lightsteelblue'
            },
            red: {
                color: 'red'
            }
        };
        let instructions = <h5>Swipe to Redeem</h5>;
        let content = [];

        switch (item) {
            case 'tenPercentOff' :
                content = [instructions, <div><p style={style.red}>(0) 10% off PURE Rewards voucher</p></div>];
                break;
            case 'fiveDollars' :
                content = [instructions, <div><p style={style.red}>(0) $5 voucher</p></div>];
                break;
            case 'tenDollars' :
                content = [instructions, <div><p style={style.red}>(0) $10 voucher</p></div>];
                break;
            default:
                content = [<div>d'oh!</div>];
        }

        return (<div className='static-modal'>
            <Modal title={item}
                   enforceFocus={false}
                   backdrop={false}
                   animation={false}
                //container={mountNode}
                   onRequestHide={() => {}}>
                <div className='modal-body'>
                    {content}
                </div>
                <div className='modal-footer'>
                    <Button>Close</Button>
                </div>
            </Modal>
        </div>);
    }
});

export default VouchersMenu;
