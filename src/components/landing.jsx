import React from 'react';
import TopNav from './topNav.jsx';
import MainMenu from './mainMenu.jsx';
import {Modal, Button, OverlayMixin} from 'react-bootstrap';

const LandingPage = React.createClass({

    mixins: [OverlayMixin],

    getInitialState() {
        return {showOffers: true, isModalOpen: true};
    },

    render() {

        const styles = {
            outerContainer: {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center'
            }
        };

        return (
            <div style={styles.outerContainer}>
                <TopNav />
                <MainMenu />
            </div>
        );
    },

    handleModalClose() {
        this.setState({menuItemSelected: false, isModalOpen: false});
        // this.refs.modal.hide();
    },

    // This is called by the `OverlayMixin` when this component
    // is mounted or updated and the return value is appended to the body.
    renderOverlay() {
        if (!this.state.isModalOpen) {
            return <span/>;
        }

        const style = {
            container: {
                border: '1px solid silver',
                backgroundColor: 'palegreen',
                marginTop: 10
            }
        };

        const content = [
            <div style={style.container}>
                <p>
                    {'() Reminders'}
                </p>
                <ul>
                    <li>Remember to print your 10% off voucher!</li>
                </ul>
            </div>,

            <div style={style.container}>
                <p>
                    {'(50) Dailies!'}
                </p>
                <ul>
                    <li>Daily 50 points received for swiping in at Calgary, Edmonton or Yellowhead!</li>
                </ul>
            </div>,

            <div style={style.container}>
                <p>
                    {'(100) Wednesday Senior Days'}
                </p>
                <ul>
                    <li>Wednesday Senior Days! Swipe in at our other locations for more points!</li>
                </ul>
            </div>
        ];

        return (
            <div className='static-modal'>
                <Modal title='Offers!'
                       enforceFocus={false}
                       backdrop={false}
                       animation={false}
                    //container={mountNode}
                       onRequestHide={() => {}}>
                    <div className='modal-body'>
                        {content}
                    </div>
                    <div className='modal-footer'>
                        <Button onClick={this.handleModalClose}>Close</Button>
                    </div>
                </Modal>
            </div>);
    }
});
export default LandingPage;
