import React from 'react';
import LandingPage from './landing.jsx';

const Welcome = React.createClass({

    propTypes: {
        location: React.PropTypes.oneOf(['Edmonton', 'Calgary'])
    },

    getInitialState() {
        return {dataEntryFieldVisible: false};
    },

    handleIdChange() {
        const input = this.refs.idInput;
        if (input) {
            const inputDOM = React.findDOMNode(input);
            const id = inputDOM.value;
            console.log(id);
            if (id === '99999') {
                console.log('this.setState({validIdEntered: true});');
                this.setState({validIdEntered: true});
            }
        }
    },

    startInputClickHandler() {
        this.clearTimer();
        this.timer = setInterval(this.timeOut, 30*1000);
        this.setState({dataEntryFieldVisible: true});
    },
    clearTimer() {
        if (this.timer) {
            clearInterval(this.timer);
        }
    },
    timeOut() {
        this.clearTimer();
        this.setState({dataEntryFieldVisible: false});
    },

    componentDidUpdate() {
        const input = this.refs.idInput;
        if (input) {
            React.findDOMNode(input).focus();
        }
    },

    render() {
        const style = {
            container: {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                width: '100vw'
            },
            header: {
                margin: 'auto',
                marginTop: 40
            },
            firstParagraph: {
                margin: 'auto',
                marginTop: 40
            },
            secondParagraph: {
                margin: 'auto',
                marginTop: 30
            },
            child: {
                margin: 'auto',
                marginTop: 20
            },
            instructions: {
                margin: 'auto',
                marginTop: 100,
                marginBottom: 80,
                fontWeight: 700,

                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                width: 300
            },
            image: {
                height: 100,
                width: 400,
                margin: 'auto'
            }
        };
        const locationString = 'Casino ' + this.props.location;
        const locationImageURL = './Casino' + this.props.location + '.png';
        let instructions;
        if (!this.state.dataEntryFieldVisible) {
            instructions = <p style={style.child}>Tap to begin</p>;
        } else {
            instructions = [
                <p style={style.child}>Please swipe your Pure Rewards Card</p>,
                <input style={style.child} type='search' ref="idInput" onChange={this.handleIdChange} />
            ];
        }
        if (this.state.validIdEntered) {
            return (<LandingPage/>);
        } else {
            return (
                <div style={style.container} onClick={this.startInputClickHandler}>
                    <h1 style={style.header}>Welcome to {locationString}</h1>
                    <p style={style.firstParagraph}>Welcome to {locationString}!</p>
                    <p style={style.secondParagraph}>Pure Service, Pure Rewards, Pure Entertainment!</p>
                    <p style={style.child}>With Pure Canadian Gaming!!</p>
                    <p style={style.child}>Good Luck!!</p>
                    <div style={style.instructions}>
                        {instructions}
                    </div>
                    <img style={style.image} src={locationImageURL}></img>
                </div>
            );
        }
    }
});
export default Welcome;
