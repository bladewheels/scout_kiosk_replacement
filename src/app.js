import React from 'react';
import FastClick from 'fastclick';
import 'intl/Intl';
import _ from 'lodash';
import Config from './Config';
import App from './components/app.jsx';
import messages from './messages/messages';

const defaultLocale = 'en-CA';

new Promise((resolve) => {
    if (window.addEventListener) {
        window.addEventListener('DOMContentLoaded', resolve);
    } else {
        window.attachEvent('onload', resolve);
    }
}).then(() => {
    FastClick.attach(document.body);
}).then(() => {
    const query = _.zipObject(
        location.search.substr(1).split('&').map((p) => p.split('=')));

    const app = (
        <App
            config={Config.forEnv(query.env)}
            locales={[defaultLocale]}
            messages={messages}
            />
    );
    React.render(app, document.querySelector('#app'));
});
