import {IntlMixin, FormattedNumber, FormattedDate, FormattedTime, FormattedRelative} from 'react-intl';
import FormattedMessage from '../components/FormattedMessageWrapper.jsx';

const ReactIntlWrapper = {
    IntlMixin,
    FormattedMessage,
    FormattedNumber,
    FormattedDate,
    FormattedTime,
    FormattedRelative
};

export default ReactIntlWrapper;
