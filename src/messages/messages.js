export default {
    app: {
        placeholder: 'This is the App component.'
    },
    appSelector: {
        title: 'Trial Balance',
        trialBalanceMenuItemTitle: 'Trial Balance',
        booksToTaxMenuItemTitle: 'Books to Tax'
    },
    stage: {
        title: 'Tax Review',
        placeholder: 'This is the Stage component.',
        totalAssets: 'TOTAL ASSETS',
        totalLiabilitiesAndEquity: 'TOTAL LIABILITIES AND EQUITY',
        totalIncome: 'NET INCOME'
    },
    content: {
        trialBalanceTitle: 'Trial Balance',
        balanceSheetTitle: 'Balance Sheet',
        balanceSheet: 'Balance Sheet content',
        incomeStatementTitle: 'Income Statement',
        incomeStatement: 'Income Statement content',
        taxGroupingsTitle: 'Tax Groupings'
    },
    booksToTaxFiller: {
        openTrowser: 'Open Books to Tax'
    },
    createBookAdjustmentButton: {
        makeAdjusmtent: 'Add book adjustment'
    },
    trialBalanceGrid: {
        columnGroups: {
            currentYear: 'CY Ending Balance',
            priorYear: 'PY Ending Balance'
        },
        columns: {
            account: 'Account',
            type: 'Type',
            currentYear: {
                debit: 'Debit',
                credit: 'Credit'
            },
            priorYear: {
                debit: 'Debit',
                credit: 'Credit'
            }
        },
        loader: 'Loading...'
    },
    footer: {
        accountingMethodWithAsOfDate: '{accountingMethod} basis, as of {endDate}'
    }
};
