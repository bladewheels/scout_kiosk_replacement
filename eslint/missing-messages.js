require('babel/register');
var messagesStore = require('../src/messages/messages.js');
var _ = require('lodash');

module.exports = function (context) {
    var errorMessage = 'Expected message to be in the messages store: ';

    var isInMessageStore = function (keyString) {
        return _.has(messagesStore, keyString);
    };

    return {
        CallExpression: function (node) {
            var messageKey;
            if (node.callee.property && node.callee.property.name === 'getIntlMessage') {
                messageKey = node.arguments[0].value;
                if (messageKey && !isInMessageStore(messageKey)) {
                    context.report(node, errorMessage + messageKey);
                }
            }
        }
    };
};
