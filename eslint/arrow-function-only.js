module.exports = function (context) {
    var message = 'Expected an arrow function';

    var isObjectOrClassMethod = function (parent) {
        var type = parent.type;
        return type === 'MethodDefinition' || (type === 'Property' && parent.method);
    };

    var isCallExpression = function (type) {
        return type === 'CallExpression';
    };

    var isVariableDeclarator = function (type) {
        return type === 'VariableDeclarator';
    };

    return {
        FunctionDeclaration: function (node) {
            context.report(node, message);
        },

        FunctionExpression: function () {
            var parent = context.getAncestors().pop(),
                type = parent.type;

            if (isVariableDeclarator(type) || isCallExpression(type) || !isObjectOrClassMethod(parent)) {
                context.report(parent, message);
            }
        }
    };
};
