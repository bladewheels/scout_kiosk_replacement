module.exports = function (context) {
    var message = 'Expected arrow function syntax to be "(parameters) => function body": ',
        syntaxRegex = /\(.*\)\s?=>\s?{?(.|\s)*}?/;

    return {
        ArrowFunctionExpression: function (node) {
            var tokens = context.getTokens(node),
                tokenString = tokens.reduce(function (previousValue, currentValue) {
                    return previousValue + currentValue.value;
                }, '');

            if (!syntaxRegex.test(tokenString)) {
                context.report(node, message + tokenString);
            }
        }
    };
};
