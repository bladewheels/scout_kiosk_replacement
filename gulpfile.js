/*
 * React.js Starter Kit
 * Copyright (c) 2014 Konstantin Tarkus (@koistya), KriaSoft LLC.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

'use strict';

// Include Gulp and other build automation tools and utilities
// See: https://github.com/gulpjs/gulp/blob/master/docs/API.md
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');
var path = require('path');
var runSequence = require('run-sequence');
var webpack = require('webpack');
var argv = require('minimist')(process.argv.slice(2));

// Settings
var watch = false;
var browserSync;

// The default task
gulp.task('default', ['sync']);

// Clean output directory
gulp.task('clean', del.bind(
    null, ['.tmp', 'build/*', '!build/.git'], {dot: true}
));

// Bundle
gulp.task('bundle', function (cb) {
    var started = false;
    var config = require('./webpack.config.js');
    var bundler = webpack(config);

    var bundle = function (err, stats) {
        if (err) {
            throw new $.util.PluginError('webpack', err);
        }

        if (argv.verbose) {
            $.util.log('[webpack]', stats.toString({colors: true}));
        }

        if (!started) {
            started = true;
            return cb();
        }
    };

    if (watch) {
        bundler.watch(200, bundle);
    } else {
        bundler.run(bundle);
    }
});

// Copy any unpackaged files directly to the output folder
gulp.task('copy-static-files', function () {
    gulp.src(['./src/index.html', './src/resources/img/*.png'])
        // Perform minification tasks, etc here
        .pipe(gulp.dest('./build'));
});

gulp.task('copy-githooks', function () {
    gulp.src('./githooks/*').pipe(gulp.dest('./.git/hooks'));
});

// Inspect source code for duplication
gulp.task('inspect', function () {
    return gulp.src(['src/**/*.js', '!src/**/__tests__/**/*.js'])
        .pipe($.jsinspect({
            'threshold': 0,
            'identifiers': true,
            'suppress': 0
        }));
});

// Run eslint and jest
// TODO: Revisit when gulp plugins for eslint/jest stabilize, use those instead.
gulp.task('test', ['inspect'], $.shell.task([
    'npm test'
]));

// Build the app from source code
gulp.task('build', ['clean', 'test'], function (cb) {
    runSequence(['bundle', 'copy-static-files', 'copy-githooks'], cb);
});

// Build and start watching for modifications
gulp.task('build:watch', function (cb) {
    watch = true;
    runSequence('build', cb);
});

// Launch BrowserSync development server
gulp.task('sync', ['build:watch'], function (cb) {
    browserSync = require('browser-sync');

    browserSync({
        logPrefix: 'RSK',
        notify: false,
        // Run as an https by setting 'https: true'
        // Note: this uses an unsigned certificate which on first access
        //       will present a certificate warning in the browser.
        https: true,
        startPath: 'index.html',
        server: {
            baseDir: 'build',
            middleware: function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                next();
            }
        },
        port: 8080
    }, cb);

    process.on('exit', function () {
        browserSync.exit();
    });

    gulp.watch(['build/**/*.*'],
        function (file) {
            browserSync.reload(path.relative(__dirname, file.path));
        });
});

// Deploy to GitHub Pages
gulp.task('deploy', function () {

    // Remove temp folder
    if (argv.clean) {
        var os = require('os');
        var repoPath = path.join(os.tmpdir(), 'tmpRepo');
        $.util.log('Delete ' + $.util.colors.magenta(repoPath));
        del.sync(repoPath, {force: true});
    }

    return gulp.src('build/**/*')
        .pipe($.if('**/robots.txt', !argv.production ?
            $.replace('Disallow:', 'Disallow: /') : $.util.noop()))
        .pipe($.ghPages({
            remoteUrl: 'https://github.com/{name}/{name}.github.io.git',
            branch: 'master'
        }));
});

// Run PageSpeed Insights
gulp.task('pagespeed', function (cb) {
    var pagespeed = require('psi');
    // Update the below URL to the public URL of your site
    pagespeed.output('example.com', {
        strategy: 'mobile'
        // By default we use the PageSpeed Insights free (no API key) tier.
        // Use a Google Developer API key if you have one: http://goo.gl/RkN0vE
        // key: 'YOUR_API_KEY'
    }, cb);
});
