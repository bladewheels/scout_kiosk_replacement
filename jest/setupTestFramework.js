import React from 'react/addons';
import {IntlMixin} from '../src/lib/ReactIntlWrapper.js';

(function () {
    const T = React.addons.TestUtils;

    window.render = function (element) {
        const wrap = (obj) => {
            if (obj.map) { // For some reason, _.isArray(obj) is always false.
                return obj.map((scried) => wrap(scried));
            } else {
                return {
                    getComponent() {
                        return obj;
                    },
                    isDOMComponent() {
                        return T.isDOMComponent(obj);
                    },
                    displayName() {
                        return obj.constructor.displayName;
                    },
                    textContent() {
                        return obj.getDOMNode().textContent;
                    },
                    attributeValue(name) {
                        return obj.getDOMNode().getAttribute(name);
                    },
                    getDOMNode() {
                        return obj.getDOMNode();
                    },
                    scryTag(tag) {
                        return wrap(T.scryRenderedDOMComponentsWithTag(obj, tag));
                    },
                    scryClass(cls) {
                        return wrap(T.scryRenderedDOMComponentsWithClass(obj, cls));
                    },
                    scryComponent(type) {
                        return wrap(T.scryRenderedComponentsWithType(obj, type));
                    },
                    findTag(tag) {
                        return wrap(T.findRenderedDOMComponentWithTag(obj, tag));
                    },
                    findClass(cls) {
                        return wrap(T.findRenderedDOMComponentWithClass(obj, cls));
                    },
                    findComponent(type) {
                        return wrap(T.findRenderedComponentWithType(obj, type));
                    }
                };
            }
        };
        return wrap(T.renderIntoDocument(element));
    };

    window.expectMissingPropType = function (propNames, getComponent) {
        const original = console.warn;
        spyOn(console, 'warn');
        getComponent();
        expect(console.warn).toHaveBeenCalled();
        expect(console.warn.calls.length).toBe(propNames.length);
        console.warn.calls.forEach((call, i) => {
            expect(call.args[0]).toContain(`\`${propNames[i]}\``);
        });
        console.warn = original;
    };

    window.mock = {
        methods: function (cls, methods) {
            const prototype = cls.prototype;
            const originals = {};
            for (let name in methods) {
                const fn = methods[name];
                originals[name] = prototype[name];
                prototype[name] = jest.genMockFn().mockImpl(fn);
            }
            return {
                restore() {
                    for (let name in originals) {
                        const fn = originals[name];
                        if (typeof fn === 'undefined') {
                            delete prototype[name];
                        } else {
                            prototype[name] = fn;
                        }
                    }
                },
                andRun(fn) {
                    fn();
                    this.restore();
                }
            };
        },
        components: function (...classes) {
            classes.forEach(function (cls) {
                cls.prototype.render = jest.genMockFn();
            });
        }
    };

    jasmine.getEnv().beforeEach(function () {
        this.addMatchers({
            toBeEmpty: function () {
                return this.actual.length === 0;
            },
            toContainComponents: function (...types) {
                const component = this.actual;
                this.message = function () {
                    const children = types.map(type => `<${type.displayName}>`).join(', ');
                    return [
                        `Expected <${component.displayName()}> to contain: ${children}`,
                        `Expected <${component.displayName()}> to not contain: ${children}`
                    ];
                };
                return types.every(type => component.scryComponent(type).length > 0);
            }
        });
    });

    IntlMixin.getIntlMessage.mockImpl(function (id) {
        return id;
    });
}());
